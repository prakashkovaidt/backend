 /* jshint esversion: 6 */
 var express = require('express');
 var employee = require('../services/empService');
 var router = express.Router();
 router.post('/', async (req, res) => {
   //console.log(req.body);
   //return res.json(req.body);
   try {
     await employee.saveEmployee(req, res);
   } catch (err) {
     console.log('Error on ' + err.message);
   }
 });
 router.get('/', async (req, res) => {
   //console.log(req.body);
   //return res.json(req.body);
   try {
     await employee.getEmployee(req, res);
   } catch (err) {
     console.log('Error on ' + err.message);
   }
 });
 module.exports = router;