var express = require('express');
var menulist = require('../services/menuService');
var router = express.Router();
router.get('/', async function (req, res) {
    //console.log('Got');
    res.setHeader('content-type', 'application/json');
    await menulist.getMainMenu(req, res);

});
module.exports = router;