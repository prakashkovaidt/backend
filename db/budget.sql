-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2020 at 02:07 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `budget`
--

-- --------------------------------------------------------

--
-- Table structure for table `mainmenu`
--

CREATE TABLE `mainmenu` (
  `menuid` int(3) NOT NULL,
  `menuname` varchar(50) NOT NULL,
  `deleted` char(1) NOT NULL,
  `adduser` varchar(20) NOT NULL,
  `adddate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mainmenu`
--

INSERT INTO `mainmenu` (`menuid`, `menuname`, `deleted`, `adduser`, `adddate`) VALUES
(1, 'NEW ENTRY', 'N', 'PRAKASH', '2020-01-02'),
(2, 'EXPENSE', 'N', 'PRAKASH', '2020-01-02'),
(3, 'INCOME', 'N', 'PRAKASH', '2020-01-02'),
(4, 'REPORTS', 'N', 'PRAKASH', '2020-01-02'),
(5, 'ASSET PURCHASE', 'N', 'PRAKASH', '2020-01-02'),
(6, 'GENERAL', 'N', 'PRAKASH', '2020-01-02'),
(7, 'DONATIONS', 'N', 'PRAKASH', '2020-01-02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mainmenu`
--
ALTER TABLE `mainmenu`
  ADD PRIMARY KEY (`menuid`),
  ADD KEY `menuid` (`menuid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mainmenu`
--
ALTER TABLE `mainmenu`
  MODIFY `menuid` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
