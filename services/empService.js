const pool = require('../config/dbconfig');
var employes = require('../models/employees');
module.exports.saveEmployee = async (req, res, callback) => {

  //res.json(req.body);
  var employes = {
    EMPSRNO: null,
    EMPNAME: req.body.fullName,
    EMPMOBI: req.body.mobile,
    EMPPOST: req.body.position,
    EMPCODE: req.body.Empcode
  };
  //console.log(employes);
  await pool.query("insert into employees set ?", [employes], (err, rows, fields) => {
    if (err) {
      return res.json({
        'error': true,
        'message': err.errno + ' - ' + err.sqlMessage,
        'query': err.sql
      });
    }
    return res.json({
      'error': false,
      'message': 'User Added',
      'fullName': req.body.fullName
    });

  });
  //return Datarows;
}
module.exports.getEmployee = async (req, res, callback) => {

  //res.json(req.body);
  var employes = {
    EMPSRNO: null,
    EMPNAME: req.body.fullName,
    EMPMOBI: req.body.mobile,
    EMPPOST: req.body.position,
    EMPCODE: req.body.Empcode
  };
  //console.log(employes);
  await pool.query("select * from  employees where deleted = 'N' ", (err, rows, fields) => {
    if (err) {
      return res.json({
        'error': true,
        'message': err.errno + ' - ' + err.sqlMessage,
        'query': err.sql
      });
    }
    return res.json(rows);

  });
  //return Datarows;
}