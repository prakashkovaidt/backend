const server = require('./config/webserver');
var dbCon = require('./config/dbconn');
(async () => {
    try {
        await dbCon.connect();
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
    try {
        await server.WebInitialize();
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
})();
async function Calldown(e) {
    let err = e;
    try {
        console.log('Disconnecting to Database Server... ');
        await dbCon.down();
    } catch (err) {
        console.log(err);
    }
    try {
        console.log('Gracefull Shutdown for Web Server... ');
        await server.down();
    } catch (err) {
        console.log(err)
    }
    err = err || e;
    if (err) {
        process.exit(1);
    } else {
        process.exit(0);
    }
}
process.on('SIGTERM', () => {
    console.log('Received SIGTERM');

    Calldown();
});

process.on('SIGINT', () => {
    console.log('Received SIGINT');

    Calldown();
});

process.on('uncaughtException', err => {
    console.log('Uncaught exception');
    console.error(err);

    Calldown(err);
});