const express = require('express');
const http = require('http');
const cors = require('cors');
const path = require('path');
const morgan = require('morgan');
const fs = require('fs');
const momentTz = require('moment-timezone');
var cookieParser = require('cookie-parser');
var home = require('../routes/home');
var remove = require('../routes/remove');
var empl = require('../routes/employee');
require('dotenv').config();
//console.log(process.env);
// create a write stream (in append mode)
morgan.token('pid', (req, res) => {
  return process.pid;
});
morgan.token('date', (req, res, tz) => {
  return momentTz().tz('Asia/Kolkata').format("dddd, MMMM Do YYYY, h:mm:ss a");
});
morgan.format('myformat', ':pid - :remote-addr - :remote-user [:date[Asia/Kolkata]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" - :response-time ms');
//:remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {
  flags: 'a'
});

let Server;

function WebInitialize() {
  return new Promise((resolve, reject) => {
    var app = express();
    app.use(cors());
    app.use(morgan('myformat', {
      stream: accessLogStream
    }));
    app.use(express.json());
    app.use(express.urlencoded({
      extended: true
    }));
    app.use(cookieParser());
    //app.use(bodyParser());
    app.use(express.static(path.join(__dirname, 'public')));
    //app.use(morgan('combined'));
    //app.use('/', home);
    app.use(['/home', '/'], home);
    app.use('/remove', remove);
    app.use('/employee', empl);
    Server = http.createServer(app);
    Server.listen(process.env.WWW_PORT || 3001, err => {
      if (err) {
        reject(err);
        return;
      }
      console.log(`Web Server is listening on ${ process.env.WWW_PORT }`);
      resolve();

    });

  });
}
module.exports.WebInitialize = WebInitialize;

function down() {
  return new Promise((resolve, reject) => {
    Server.close((err) => {
      if (err) {
        reject(err);
        return;
      }
      resolve();
    });
  });
}

module.exports.down = down;