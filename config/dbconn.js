var dbConn = require('./dbconfig');
let loginCount=0;
let dbDown;
function HandleReconnect(){
    if(loginCount==process.env.DB_RECONNECT){
        down();
    }
    dbConn.getConnection(function(err, connection) {
        if(err){
            if(err.code==='ECONNREFUSED'){
                console.log('Database Server is re-connecting...');
                //connection.release();
                //process.exit(0);
                setTimeout(()=>{
                    loginCount++;
                    HandleReconnect()
                },2000);
            }
        }else{
            console.info("Re-connecting to mysql :OK");
            dbDown=connection;
            connection.release();
        }
      });
}
module.exports.connect = ()=>{
    dbConn.getConnection(async function (err, connection) {
        if(err){
            if(err.code==='ECONNREFUSED'){
                console.log('Database Server is not connecting...');
                //connection.release();
                //process.exit(0);
                //setTimeout(()=>{
                   await HandleReconnect();
               // },2000);
              
            }
            
        }else{
        console.info("Connecting to mysql :OK");
        dbDown=connection;
        connection.release();
        //console.log(dbConn._freeConnections.indexOf(connection))
             
           
        }
      });
}
function down(){
    return new Promise(( resolve,reject)=>{
        if(dbConn._freeConnections.indexOf(dbDown)== -1){
            dbDown.release((err)=>{
                if(err){
                    console.log(err);
                    reject()
                    return;
                }
                resolve();
            });
        }
        resolve();

    });
        
}
module.exports.down=down;
