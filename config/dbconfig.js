var mysql = require('mysql');
var pool  = mysql.createPool({
    connectionLimit : 100, //important
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'budget',
    debug    :  false
});
//console.log(pool);

module.exports=pool;